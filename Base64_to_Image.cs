public async static Task<Image> convertToImage(String strimage)
        {
            Image image = new Image();
            try
            {
                byte[] bitmapArray;
                bitmapArray = Convert.FromBase64String(strimage);
                MemoryStream ms = new MemoryStream(bitmapArray);
                InMemoryRandomAccessStream randomAccessStream = new InMemoryRandomAccessStream();
                //将randomAccessStream 转成 IOutputStream
                var outputstream = randomAccessStream.GetOutputStreamAt(0);
                //实例化一个DataWriter
                DataWriter datawriter = new DataWriter(outputstream);
                //将Byte数组数据写进OutputStream
                datawriter.WriteBytes(bitmapArray);
                //在缓冲区提交数据到一个存储区
                await datawriter.StoreAsync();

                //将InMemoryRandomAccessStream给位图
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(randomAccessStream);

                image.Source = bitmapImage;
            }
            catch 
            {

            }
            return image;
        }